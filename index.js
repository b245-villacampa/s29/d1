// db.users.insertMany([
//             {
//                 firstName: "Jane",
//                 lastName: "Doe",
//                 age: 21,
//                 contact: {
//                     phone: "87654321",
//                     email: "janedoe@gmail.com"
//                 },
//                 courses: [ "CSS", "Javascript", "Python" ],
//                 department: "HR"
//             },
//             {
//                 firstName: "Stephen",
//                 lastName: "Hawking",
//                 age: 76,
//                 contact: {
//                     phone: "87654321",
//                     email: "stephenhawking@gmail.com"
//                 },
//                 courses: [ "Python", "React", "PHP" ],
//                 department: "HR"
//             },
//             {
//                 firstName: "Neil",
//                 lastName: "Armstrong",
//                 age: 82,
//                 contact: {
//                     phone: "87654321",
//                     email: "neilarmstrong@gmail.com"
//                 },
//                 courses: [ "React", "Laravel", "Sass" ],
//                 department: "HR"
//             }
//         ]);

db.users.insertOne(
            {
                firstName: "Bill",
                lastName: "Gates",
                age: 65,
                contact: {
                    phone: "12345678",
                    email: "bill@gmail.com"
                },
                courses: [ "PHP", "Laravel", "HTML" ],
                department: "operations"
            });

// [SECTION] COMPARISON QUERY OPERATORS
    //$gt / $gte operator 
        /*
            - Allows us to find documents that have field values greated than or equal to a specified value.

            Syntax:
            db.collectionName.find({field:{$gt: value}});
            db.collectionName.find({field:{$gte: value}});

        */

db.users.find({ age : {$gt:65} }); // This will return only 2 records
db.users.find({ age : {$gte:65} }); //Will return 3 records ncluding the age equal to 65.


    //$lt / $lte operator 
        /*
            - Allows us to find documents that have field values less than or equal to a specified value.

            Syntax:
            db.collectionName.find({field:{$lt: value}});
            db.collectionName.find({field:{$lte: value}});

        */
        
        db.users.find({age:{$lt:65}});
        db.users.find({age:{$lte:65}});

    //$ne operator
        /*
            -Allows us to find document that have field number values not equal to a specified value.
            - Syntax:
            db.collectionName.find({field:{$ne: {value}});
        */

     db.users.find({age:{$ne:65}});

    //$in (in between) operator
        /*
            -Allows us to find document with specific match criteria of one field using different values.
            - Syntax:
                db.collectionName.find({field:{$in:{[valueA, valueB]}}})

        */

        db.users.find({lastName:{$in:["Hawking", "Doe"]}});
        db.users.find({courses:{$in:["HTML","React"]}});


// [SECTION] LOGICAL OPERATOR

    //$or operator
    /*
        - Allows us to find document that match a single criteria from multiple provided search criteria 
        - Syntax:
            db.collectionName.find({$or:[{fieldA:valueA}, {fieldB: valueB}]});

    */ 
        db.users.find({
            $or:[
                {firstName : "Niel"},
                {age: 25}
            ]
        });

    //With comparison query operator  
        db.users.find({
            $or:[
                {firstName : "Niel"},
                {age: {$gt:30}}
            ]
        });

    // $and operator
        /*
            -allows us to find documents matching multiple criteria in a single field.
            - Syntax:
             db.collectionName.find({
                $and:[
                    {fieldA:valueA}, 
                    {fieldB: valueB}
                ]
            });
        */

        db.users.find({
            $and:[
                {age:{$ne:82}},
                {age:{$ne:76}}
            ]
        });

// Solution in activity
        db.users.find({
            $or:[
                {courses:{$in:["Laravel", "React"]}},
                {age:{$lt:80}}
            ]
        });




//[SECTION] FIELD PROJECTION
    //To help with readability of the values returned, we van include/ exclude fields from the retrieve results.

    // Inclusion
    /*
        -Allows us to include/add specific fields only when retrieving documents.
        -The value provided is 1 to denote that the field being included.
        -Syntax:
            db.users.find({criteria}, {field:1})
    */ 

        db.users.find(
            {
                firstName:"Jane"
            },
            {
                firstName:1,
                lastName:1,
                contact:1
            }
        );

    // Exclusion
    /*
        -Allows us to exclude/remove specific fields only when retrieving doucuments.
        - The value provided is 0 to denote that the field is being excluded.
         -Syntax:
            db.users.find({criteria}, {field:1})
    */

        db.users.find(
            {
                firstName:"Jane"
            },
            {
                _id: 0;
                contact:0,
                department:0
            }
        );

// Solution to the activity 

        db.users.find(
            {
                lastName:"Doe"
            },
            {
                _id:0,
                firstName: 1,
                lastName:1,
                contact:1
            }
        );

    // Supressing the _id field
        // -When iusing field projectuon, field inclusion and exclusion may not be used at the same time.
        // - Excluding the"_id field is the only exception to this rule.
        //syntax:
        //db.collectionName.find({criteria},{field: 1, _id:0})

        // Return a specific field in embedded documentrs 
            db.users.find(
            {
                firstName:"Jane"
            },
            {

            firstName:1,
            lastName:1,
            "contact.phone":1
        });

// Project Specific Elements in the returned Array
    // The $slice operator allows us to retrieve element the matches the criteria.
    // Syntax:
            // db.collectionName.find({criteria},
            //arrayFirield: {$slice: count})

            db.users.find({
                firstName:"Jane"
            },{
                courses:{$slice: [1,1]}
            }
            );

    // [SECTION] EVALUATION QUERY OPERATOR
            db.users.find({firstName:"jane"})

            // $regex operator
            /*
                -Allows us to find documents that match a specific string pattern using "regular expression"/"regex".
                -Syntax:
                    db.collectionName.find({field: {$regex:"pattern", $option:"optionValue"}});
            */

        // Case Sensitive Query
            db.users.find({firstName:{$regex:"J"}})

        // Case insensitive Query
            // $options:"$i" - used to make the regex to be case insensitive.
            db.users.find({firstName:{$regex:"Ne"}, $options:"$i"})
            